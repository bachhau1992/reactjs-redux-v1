import {createStore} from "redux";
import {status ,sort} from "./actions/index";
import myReducer from './reducers/index';
// khai báo các state


const store = createStore(myReducer);
console.log('default',store.getState());

// thuc hien con viec thay doi status
// let action={
//     type:'TOGGLE_STATUS'
// }
store.dispatch(status());


//thuc hien cong viec thay doi sort
// let actionSort ={
//     type:'SORT',
//     sort:{
//         by:'name',
//         value:-1
//     }
// }
// goi dispatcher
store.dispatch(sort({
    by:'name',
    value: -1
}))
console.log('sort',store.getState());
