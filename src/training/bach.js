// import {createStore} from "redux";
// // khai báo các state
// let initialState = {
//     status:false,
//     sort:{
//         by:'name',
//         value :1
//     }
// }
// // tạo ra reduce
// let myReducer = (state = initialState,action) =>{
//     if (action.type === 'TOGGLE_STATUS') {
//         state.status = !state.status;
//         return state;
//     }
//     if (action.type === 'SORT'){
//         state.sort = {
//             by:action.sort.by,
//             value:action.sort.value
//         }
//         return state;
//     }
//     return state;
// }
//
// const store = createStore(myReducer);
// console.log('default',store.getState());
//
// // thuc hien con viec thay doi status
// let action={
//     type:'TOGGLE_STATUS'
// }
// store.dispatch(action);
// console.log('toggle',store.getState());
//
// //thuc hien cong viec thay doi sort
// let actionSort ={
//     type:'SORT',
//     sort:{
//         by:'name',
//         value:-1
//     }
// }
// // goi dispatcher
// store.dispatch(actionSort)
//
import {createStore} from "redux";
let initialState={
    status:false,
    sort:{
        by:'name',
        value:1
    }
}
let myReducer =(state = initialState,action)=>{
    switch (action.type) {
        case 'TOGGLE_STATUS':
            return {
                ...state,
                status: !state.status
            }
        case 'SORT':
            const sort = {
                by:action.sort.by,
                value:action.sort.value
            }
            return {
                ...state,
                sort
            }
        default:
            return state;
    }
}
const store = createStore(myReducer);
console.log('default',store.getState());

let action = {
    type:'TOGGLE_STATUS'
}
store.dispatch(action)

let actionsort={
    type:'SORT',
    sort:{
        by:'name',
        value:-1
    }
}

store.dispatch(actionsort);
console.log('toggele',store.getState());
