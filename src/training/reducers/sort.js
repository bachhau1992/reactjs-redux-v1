let initialState = {
    by:'name',
    value:1
};
// tạo ra reduce
let myReducer = (state = initialState,action) =>{
    // if (action.type === 'TOGGLE_STATUS') {
    //     state.status = !state.status;
    //     return state;
    // }
    // if (action.type === 'SORT'){
    //     let { by, value } = action.sort;
    //     let status  = state; //status:state.status
    //     return{
    //         status : status,
    //         sort :{
    //             by : by,
    //             value : value
    //         }
    //     }
    // }
    // return state;
    switch (action.type) {
        case 'SORT':
            const sort={
                by:action.sort.by,
                value:action.sort.value
            }
            return{
                ...state,
                sort
            }
        default:
            return state;
    }
}
export default myReducer;